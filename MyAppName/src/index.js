import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import axios from 'axios';
import jwtDecoder from 'jwt-decode';
import store from './store';
import App from './components/App';
import {TOKEN} from './GlobalVariables';
import './components/devices/css/bootstrap.min.css';

export const token = window.localStorage.getItem('jwt');

if (token) {
   store.dispatch({
     type: "AUTH_USER"
   });
}

axios.interceptors.request.use((config) => {
    const jwt = window.localStorage.getItem('jwt');
    if (!config.headers.Authorization) {
        config.headers.Authorization = `jwt:${jwt}`;
    }
    return config;
});

render((
    <Provider store={store}>
        <App/>
    </Provider>
), document.getElementById('app'));
