import React, {Component} from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import * as actions from './authentication/actions/authActions';
import {Collapse, Navbar, Button, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink} from 'reactstrap';
import token from '../index';


export class Main extends Component {
    constructor() {
        super();

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        }
        this.onLogout = this.onLogout.bind(this);
    }

    onLogout () {
        localStorage.removeItem(token);
        this.props.userSignout();
        browserHistory.push("/signin");
    }

    toggle() {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }

    componentWillMount() {
        if (!this.props.auth) {
            browserHistory.push('/signin');
        } else {
            browserHistory.push('/devices');
        }
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-default" style={{backgroundColor: '#3d72c6'}}>
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                             </button>
                             <a className="navbar-brand" href="#" style={{color: '#eee', fontVariant: 'small-caps', fontSize: 24}}>Conctr</a>
                        </div>

                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav">
                                <li className="active" style={{textTransform: 'uppercase'}}><a href="#">Devices<span className="sr-only">(current)</span></a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <li onClick={this.onLogout}><a href="" style={{color: '#eee', textTransform: 'uppercase'}}>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                {this.props.children}
            </div>
        )
    }
}

function mapStateToProps(store) {
    return store.auth;
}

export default connect(mapStateToProps, actions)(Main);
