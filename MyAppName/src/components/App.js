import React, {Component} from 'react';
import Main from './Main';
import DeviceList from '../components/devices/components/DeviceList';
import SignInForm from '../components/authentication/components/SignInForm';
import {connect} from 'react-redux';
import {Router, Route, Link, browserHistory, IndexRoute} from 'react-router';
import * as actions from '../components/devices/actions/deviceActions';

class App extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <Router history={browserHistory}>
              <Route path="/" component={Main}>
                <Route path="devices" component={() => (<DeviceList {...this.props}/>)} />
              </Route>
              <Route path="/signin" component={SignInForm} />
            </Router>
        )
    }
}

function mapStateToProps(store) {
    return store.devices;
}

export default connect(mapStateToProps, actions)(App);
