import React, {Component} from 'react';
import {ListGroup, ListGroupItem} from 'reactstrap';

export default class Device extends Component {
    render() {
        console.log(this.props);
        return (
            <ListGroup style={{
                width: '90%',
                margin: 'auto',
                flex: 1,
                flexDirection: 'row',
                textAlign: 'center',
                marginBottom: 50
            }}>
                <ListGroupItem>Consumer id: {this.props.devices.consumer_id}</ListGroupItem>
                <ListGroupItem>Device id: {this.props.devices.id}</ListGroupItem>
                <ListGroupItem>Model id: {this.props.devices.model_id}</ListGroupItem>
            </ListGroup>
        )
    }
}
