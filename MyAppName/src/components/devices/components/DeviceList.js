import React, {Component} from 'react';
import Device from './Device';
import {Button} from 'reactstrap';
import {browserHistory} from "react-router";
import store from '../../../store';
import {USER_SIGNOUT} from '../../authentication/actions/authActions';

export default class DeviceList extends Component {
    constructor() {
        super();

    }

    componentWillMount() {
        this.props.fetchDevices();
    }

    render() {
        console.log(this.props);
        const devices = this.props.devices.data? this.props.devices.data.map((device) => <Device key={device.id} devices={device} />) : [];

        return (
          <div>
            <div style={{clear: 'both'}}>
                {devices.length
                    ? devices
                    : "Loading devices..."}
            </div>
          </div>
        )
    }
}
