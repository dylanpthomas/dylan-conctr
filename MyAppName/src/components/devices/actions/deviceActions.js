import axios from 'axios';

import {DOMAIN} from '../../../GlobalVariables';
const APP_ID = process.env.APP_ID || '223f44a56c1248e388ed337ba6194816';

export function fetchDevices() {
    return function(dispatch) {
        axios.get(`${DOMAIN}/${APP_ID}/consumers/devices`)
            .then((response) => {
                dispatch({
                    type: 'FETCH_DEVICES_FULFILLED',
                    payload: response.data
                });
            })
            .catch((err) => {
                dispatch({
                    type: 'FETCH_DEVICES_REJECTED',
                    payload: err
                });
            })
    }
}
