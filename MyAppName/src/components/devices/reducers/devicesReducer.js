export default function reducer(state = {
    devices: [],
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case "FETCH_DEVICES":
            {
                return Object.assign({}, state, {
                    fetching: true
                });
            }
        case "FETCH_DEVICES_FULFILLED":
            {
                return Object.assign({}, state, {
                    fetching: false,
                    fetched: true,
                    devices: action.payload
                });
            }
        case "FETCH_DEVICES_REJECTED":
            {
                return Object.assign({}, state, {
                    fetching: false,
                    error: action.payload
                });
            }
    }

    return state;
}
