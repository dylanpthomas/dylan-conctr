import {combineReducers} from 'redux';
import devices from './devicesReducer';
import auth from '../../authentication/reducers/authReducer';

const store = combineReducers({
    devices,
    auth
});

export default store;
