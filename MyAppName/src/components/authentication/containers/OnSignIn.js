import React from 'react';
import axios from 'axios';
import {DOMAIN} from '../../../GlobalVariables';
const APP_ID = process.env.APP_ID || '223f44a56c1248e388ed337ba6194816';
import {browserHistory} from 'react-router';

export const loginGoogle = (response) => {
    return (dispatch) => {
        console.log('Access token: ' + response.getAuthResponse(true).access_token);
        axios.post(DOMAIN + '/' + APP_ID + '/consumers/oauth/login', {
            'provider': 'google'
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'oth:' + response.getAuthResponse(true).access_token
            }
        }).then(function(response) {
            console.log(response.data);
            localStorage.setItem('jwt', response.data.jwt);
            dispatch({type: 'AUTH_USER'});
            browserHistory.push('devices');
        }).catch(function(error) {
            console.log(error);
        });
    }
}

export const registerGoogle = (response) => {
    return (dispatch) => {
        console.log('Access token: ' + response.getAuthResponse(true).access_token);
        axios.post(DOMAIN + '/' + APP_ID + '/consumers/oauth/register', {
            'provider': 'google'
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'oth:' + response.getAuthResponse(true).access_token
            }
        }).then(function(response) {
            localStorage.setItem('jwt', response.data.jwt);
        }).catch(function(error) {
            console.log(error);
        });
    }
}
