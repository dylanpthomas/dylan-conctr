import React, {Component} from 'react';
import axios from 'axios';
import {connect} from "react-redux";
import {Jumbotron, Button} from 'reactstrap';
import GoogleLogin from 'react-google-login';
import * as actions from '../containers/OnSignIn';

export class SignInForm extends Component {
    render() {

        const error = (response) => {
            console.error(response);
        }

        return (
            <div>
                <Jumbotron style={{textAlign: 'center'}}>
                    <div>
                        <div style={{margin: 10, display: 'inline'}}>
                            <GoogleLogin clientId="235699374806-sfqba6ienv25cjsv76govdepei2en18n.apps.googleusercontent.com" buttonText="Login" onSuccess={this.props.loginGoogle} onFailure={error} style={{
                                backgroundColor: '#676AB0',
                                borderColor: '#EEE',
                                borderStyle: 'ridge',
                                color: 'white',
                                width: 140,
                                borderRadius: 4,
                                textTransform: 'uppercase',
                                fontSize: 20
                            }}/>
                        </div>
                        <div style={{margin: 10, display: 'inline'}}>
                            <GoogleLogin clientId="235699374806-sfqba6ienv25cjsv76govdepei2en18n.apps.googleusercontent.com" buttonText="Register" onSuccess={this.props.registerGoogle} onFailure={error} style={{
                                backgroundColor: '#e15d5e',
                                borderColor: '#EEE',
                                borderStyle: 'ridge',
                                color: 'white',
                                width: 140,
                                borderRadius: 4,
                                textTransform: 'uppercase',
                                fontSize: 20
                            }}/>
                        </div>
                    </div>
                </Jumbotron>
                {this.props.children}
            </div>
        )
    }
}

export default connect(null, actions)(SignInForm);
