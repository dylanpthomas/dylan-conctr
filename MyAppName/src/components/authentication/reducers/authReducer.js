export default function reducer(state = {
    auth: false
}, action) {
    switch (action.type) {
        case "AUTH_USER":
            {
                return Object.assign({}, state, {
                    auth: true
                })
            }
        case "USER_SIGNOUT":
            {
                return Object.assign({}, state, {
                    auth: false
                })
            }
        default:
          return state;
    }

    return state;
}
