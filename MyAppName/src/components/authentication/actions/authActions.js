export function authUser() {
  return {
    type : 'AUTH_USER',
  }
}

export function userSignout() {
  return {
    type : 'USER_SIGNOUT',
  }
}
